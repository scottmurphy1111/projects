<!DOCTYPE html>
<head>
	<title>Clients MGR</title>

	<link rel="stylesheet" type="text/css" href="css/responsive.gs.12col.css">
	<link href="stylesheets/screen.css" media="screen, projection" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
	<script type="text/javascript" src="js/respond.min.js"></script>


</head>
<body>
	<section class="container">
		<header class="col span_12">
			<h1>Client Information</h1>
		</header>
		<section class="main-container">
			<section class="col span_10">
				<form id="client-info">
					<fieldset>
						<label>Company Name:</label>
						<label>Address:</label>
						<label>City:</label>
						<label>State:</label>
						<label>Zip:</label>
					</fieldset>
					<fieldset>
						<h2>Contacts</h3>
						<label>Name:</label>
						<label>Email:</label>
						<label>Phone:</label>
						<label>URL:</label>
					</fieldset>
				</form>
			</section>
			<aside class="col span_2">
				<h2>Actions</h2>
				<ul class="actions-client">
					<li><a href="#">+New Contact</a></li>
					<li><a href="#">+Edit</a></li>
					<li><a href="#">+Delete</a></li>
				</ul>
			</aside>
			<section id="creds-area" class="col span_12">
				<h2>Credentials</h2>
				<ul>
					<li>Name</li>
					<li>Username</li>
					<li>Password</li>
					<li>Notes</li>
					<li>Edit</li>
				</ul>
				<a href="#">+New Cred</a>
			</section>
		</section>
	</section>
</body>
</html>